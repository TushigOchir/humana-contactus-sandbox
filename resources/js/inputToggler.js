/***************************************************************************************************************
 * inputToggler.js
 * Date: 10/06/2017
 * This file contains logic for showing/hiding extra form fields depending on user input in the form
 ***************************************************************************************************************/
/************************
 * All constants are here
 ************************/
 /**
  * select input options for 'What would you like to do?'
	* if you change the values in the HTML, be sure to update here as well.
	* NOTE: if the value here doesn't match the corresponding <option> element value of the <value> attribute,
	*			  exception 'invalidSelectOption' is thrown.
  */
 const OPTION_QUOTE = 'Get a quote';
 const OPTION_WELLNESS = 'Learn more about wellness';
 const OPTION_DISCUSS_PRODUCTS = 'Discuss our products/services';
 const OPTION_CUSTOMER_SUPPORT = 'Get customer support';
 const EMAIL_REGEX = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
/**
 * @ALPHABETIC_DASH_REGEX : used for first name / last name input validation
 * 												matches any text that has only alphabetic
 *												characters and dash(-).
 */
const ALPHABETIC_DASH_REGEX = /^[a-zA-Z-]+$/;

const ALPHABETIC_DASH_SPACE_REGEX = /^[a-zA-Z-\s]+$/;

/**
 * @EMAIL_EXTRA_CHARS_REGEX : used for email additional character validation
 *												 matches text that is constructed from any of the
 *												 characters within the brackers (i.e. [...])
 */
const EMAIL_EXTRA_CHARS_REGEX = /^[0-9A-Za-z.,~!@#$%^&*()]+$/;

/**
 * Entry point
 */
$(document).ready(() => {
  // create custom Jquery selectors
  createCustomSelectors();
	// registers all DOM events
	initEventHandlers();
	// applies formatting to input values
	formatInputs();
	// adds custom input validations to the jquery validation plug-in
	initValidationMethods();
	// validate the form
	initFormValidation('elqForm');
});

/**
 * adds event handlers to DOM elements
 * 1. dynamic input field generation
 * 2. DOM input value updates
 */
function initEventHandlers() {
	/*
	 * dynamic input field generation handlers
	 */
  applyInputRestrictions();
	displayOnVisitorReason();
	displayOnRadioBtn('broker-yes', 'broker-no', 'show-on-broker');
	displayOnRadioBtn('broker-contact-yes', 'broker-contact-no', 'show-on-broker-contact');
	displayOnRadioBtn('employee-benefit-yes', 'employee-benefit-no', 'show-on-employee-benefit');
  registerValidationOnDropDown();
}

/**
 * forces input elements to accept/display only valid characters.
 * For example, 'First name' will only accept alphabetic characters and dash,
 * and will not accept numbers.
 */
function applyInputRestrictions() {
  /*
   * All input elements whose valid input is alphabetic characters and dash,
   * will only accept valid characters. Other characters like *&&() etc...
   * will not be accepted and won't be visible
   */
  $('input:alphabetic-dash').on('keydown', function(event) {
    const charPressed = event.key;
    if (!ALPHABETIC_DASH_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });

  /*
   * zip code will only accept numbers as input
   */
  new Cleave('#zipPostal', {
    numeral: true,
    delimiter: ''
  });
}

/**
 * Creates custom Jquery selectors
 */
function createCustomSelectors() {
  /*
   * :pe-not-filled
   * intended usage: selects all input elements that are not filled in
   */
  $.extend($.expr[':'], {
      'pe-not-filled': function(el) {
        const $el = $(el);
        /*
         * A radio button is not filled-in if none of the buttons in the Group
         * is checked
         */
        if ($el.prop('type') === 'radio') {
          return $(`input[name=${$el.prop('name')}]:checked`).length === 0;
        }
        /*
         * For other types of input elements, we can simply check that they have
         * no value
         */
        if (!$el.val()) return true;
        return $el.val().trim().length === 0;
      },

      'alphabetic-dash': function(el) {
        const $el = $(el);
        return $el.hasClass('alphabetic-dash');
      }
  });
}

/**
 * adds formatting to input values
 */
function formatInputs() {
	/*
	 * formats user phone number
	 * 2345678900 becomes 234-567-8900
	 */
	new Cleave('.input-phone', {
		phone: true,
		phoneRegionCode: 'US',
		blocks: [3, 3, 4],
		delimiter: '-',
		stripLeadingZeroes: true
	});

  /*
	 * formats agent phone number
	 * 2345678900 becomes 234-567-8900
	 */
	new Cleave('.input-agent-phone', {
		phone: true,
		phoneRegionCode: 'US',
		blocks: [3, 3, 4],
		delimiter: '-',
		stripLeadingZeroes: true
	});

	/*
	 * formats number of employees
	 * 1000 -> 1,000
	 * 1000000 -> 1,000,000
	 */
	new Cleave('.input-number-employees', {
		numeral: true
	});

	/*
	 * formats the date field for Employee benefit renewal date
   * format: mm/dd/YYYY
	 */
	new Cleave('.input-employee-benefit-renewal-date', {
		date: true,
		datePattern: ['m', 'd', 'Y']
	});
}

/**
 * adds any custom validation methods to the jquery validation plug-in
 */
function initValidationMethods() {
	$.validator.addMethod('isAlphabeticDash', isAlphabeticDash, 'Please enter alphabetic characters and -');
	$.validator.addMethod('isEmailValid', isEmailValid, 'Following characters are valid:  .,~!@#$%^&*()_+-={}|[]\:”;’<>/?');
	$.validator.addMethod('isNonZero', isNonZero, 'Please enter non-zero value');
  $.validator.addMethod('isValidPhone', isValidPhone, 'Please enter a complete phone number');
  $.validator.addMethod('isAlphabeticDashSpace', isAlphabeticDashSpace, 'only alphabetic, -, and space characters are allowed');
}

/**
 * validates the specified form
 * @param (string) : id of the form to validate
 */
function initFormValidation(formId) {
	const validator = $(`#${formId}`).validate({
    validClass: 'valid',
    errorClass: 'invalid',
    /*
     * validate the input fields as the user types
     */
    onkeyup: validateFormFields,
    onclick: validateFormFields,
    onblur: validateFormFields,
    /*
     * validation rules for all the form elements
     */
		rules: {
			firstname: {
				required: true,
				minlength: 2,
				isAlphabeticDash: true
			},
			lastname: {
				required: true,
				minlength: 2,
				isAlphabeticDashSpace: true
			},
			email: {
				required: true,
				isEmailValid: true,
			},
			phone: {
				required: true,
        isValidPhone: true
			},
			'contact-method-radio': {
				required: true
			},
			company: {
				required: true,
				minlength: 2
			},
			noOfEmployees: {
				required: true,
				isNonZero: true
				//require number
			},
			zipPostal: {
				required: true,
				digits: true
			},
			state: {
				required: true
			},
			txtwhy: {
				required: true
			},
			'broker-radio': {
				required: true
			},
			'broker-contact-radio': {
				required: true
			},
			'input-agent-name': {
				required: true,
				isAlphabeticDashSpace: true
			},
			cboHelp: {
				required: true
			},
			txtGroupMemberId: {
				required: true
			}
		},
		// Error message specification
		messages: {
			firstname: {
				required: 'First name is required',
				isAlphabetic: 'Please enter alphabetic and - characters only',
				minglenght: 'Please enter at least two characters'
			},
			lastname: {
				required: 'Last name is required',
				isAlphabeticDashSpace: 'Please enter alphabetic, -, and/or space characters only',
				minglenght: 'Please enter at least two characters'
			},
			email: {
				required: 'Email address is required',
				isEmailValid: 'Please enter a valid email address'
			},
			phone: {
				required: 'Phone number is required',
        isValidPhone: 'Please enter a valid U.S. phone number'
			},
			'contact-method-radio': {
				required: 'Please select a preferred contact method'
			},
			company: {
				required: 'Company name is required',
			},
			noOfEmployees: {
				required: 'Number of employees is required',
			},
			zipPostal: {
				required: 'ZIP code is required',
				digits: 'Please enter a valid ZIP code'
			},
			state: {
				required: 'State selection is required',
			},
			txtwhy: {
				required: 'Please select an option'
			},
			'broker-radio': {
				required: 'Please select an option'
			},
			'broker-contact-radio': {
				required: 'Please select an option'
			},
			'input-agent-name': {
				required: 'Please enter agent\'s full name',
        isAlphabeticDashSpace: 'Please enter alphabetic, -, and/or space characters only'
			},
			cboHelp: {
				required: 'Please select an option'
			},
			txtGroupMemberId: {
				required: 'Please provide a Group ID'
			}
		},
    errorPlacement: function(error, element) {
      let $inputBox = $(element);
      while (!$inputBox.hasClass('input-box')) {
        $inputBox = $inputBox.parent();
      }
      // place the error message after parent container with class 'input-box'
      $inputBox.after(error);
    },
    submitHandler: function(form) {
      // show loading bar
      $('.submit-loading-bar').removeClass('hidden');
      // disable submit button
      $('.submit-btn').prop('disabled', true);
    }
	});
}

/**
 * Depending on what user select from the drop down menu,
 * form will have dynamic fields.
 */
function displayOnVisitorReason() {
	const $txtWhy = $('#txtwhy');

	/*
	 * @getQuoteFields (Jquery object) : collection of DOM elements that should be shown
	 * if user selected 'Get a quote'
	 */
	const $getQuoteFields = $('.show-on-get-quote');

	/*
	 * @wellnessFields (Jquery object) : collection of DOM elements that should be shown
	 * if user selected 'Learn more about wellness'
	 */
	const $wellnessFields = $('.show-on-learn-more-about-wellness');

	/*
	 * @productsFields (Jquery Object) : collection of DOM elements that should be
	 * shown if user selected 'Discuss our products/services'
	 */
	const $productsFields = $('.show-on-discuss-our-products');

	/*
	 * @quote$FieldsArr (Jquery object) : collection of DOM elements that should be shown
	 * if user selected 'Get customer support'
	 */
	const $customerSupportFieldsArr = $('.show-on-get-customer-support');

	/*
	 * provides mapping from user selection to deciding what DOM elements to show and hide
	 */
	const optionToBehavior = {
		[OPTION_QUOTE] : function() {
			$getQuoteFields.removeClass('hidden');
		},
		[OPTION_WELLNESS] : function() {
			$wellnessFields.removeClass('hidden');
		},
		[OPTION_DISCUSS_PRODUCTS] : function() {
			$productsFields.removeClass('hidden');
		},
		[OPTION_CUSTOMER_SUPPORT] : function() {
			$customerSupportFieldsArr.removeClass('hidden');
		}
	};
	// display dynamic input fields based on user selection
	$txtWhy.on('change', (event) => {
		// removes all dynamic fields at the beginning
		$wellnessFields.addClass('hidden');
		$productsFields.addClass('hidden');
		$customerSupportFieldsArr.addClass('hidden');
		$getQuoteFields.addClass('hidden');
		const optionPicked = $txtWhy.val();

		// makes sure option is valid
		if (optionToBehavior.hasOwnProperty(optionPicked)) {
			optionToBehavior[optionPicked]();
		} else { // option is not valid
			throw new invalidSelectOption(optionPicked);
		}
	});
}

/**
 * @param showId (string) : id of the radio button that will display extra fields if selected
 * @param hideId (string) : id of the radio button that will hide extra fields if selected
 * @param fieldClass (string) : every element with this class name will be either shown or hidden depending
 * 								on which radio button is selected
 */
function displayOnRadioBtn(showId, hideId, fieldClass) {
  // Yes button
	const $radioShow = $(`#${showId}`);
  // No button
	const $radioHide = $(`#${hideId}`);
  // elements to show/hide based on which button was pressed
	const $fieldsArr = $(`.${fieldClass}`);
	$radioShow.on('change', (event) => {
		if ($radioShow.is(':checked')) {
			$fieldsArr.removeClass('hidden');
		}
	});

	$radioHide.on('change', (event) => {
		if ($radioHide.is(':checked')) {
			$fieldsArr.addClass('hidden');
		}
	});
}

/**
 * triggers form validation when option of <select> changes
 * Had to do it outside of the jquery validation plug-in since it doesn't
 * support change event.
 */
function registerValidationOnDropDown() {
  const $allSelectElements = $('select');
  $allSelectElements.on('change', function(el) {
    validateFormFields(el.target);
  });
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is alphabetic and/or has -
 *										 false otherwise
 */
function isAlphabeticDash(value) {
	return value.search(ALPHABETIC_DASH_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is alphabetic and/or has dash('-'), and/or space(' ')
 *										 false otherwise
 */
function isAlphabeticDashSpace(value) {
  return value.search(ALPHABETIC_DASH_SPACE_REGEX) !== -1;
}
/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is 0, 1, or more characters from
 *										 the regular expression <emailExtraChars>
 *										 false otherwise
 */
function isEmailValid(value) {
	return EMAIL_REGEX.test(value);
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input we're checking
 * @return (boolean) : true if value is not zero
 *								     false otherwise
 */
function isNonZero(value) {
	return parseInt(value) !== 0;
}

/**
 * @return (boolean) : true if user enter full phone number
 *                     false otherwise
 * NOTE: this method assumes that user can only enter numbers.
 */
function isValidPhone(value) {
  // if user entered country code, full phone number consists of 14 characters
  // (11 numbers + 3 '-')
  if (value[0] === '1') {
    return value.length === 14;
  } else { // otherwise, full phone number consists of 12 characters (10 numbers + 2 '-')
    return value.length === 12;
  }
}

/**
 * Custom exception
 *
 * Current usage: thrown when selected option value for the input 'What would you like to do?'
 *								does not match candidate values.
 * What to do if this exception is thrown?
 * Solution: Double check that the <value> attribute in the HTML matches the values
 * in the javascript file (here).
 * Example:
 * HTML : <option value="Get a Quote">Get a quote</option>
 * JS : const OPTION_QUOTE = 'Get a quote';
 * In the html, value of the <value> attribute does not match the expected value in js.
 * NOTE: Expected values are found in the constants section at the top of this file.
 */
function invalidSelectOption(value) {
   this.value = value;
   this.message = 'does not match any expected value';
   this.toString = function() {
      return this.value + this.message;
   };
}

/**
 * this method validates form input field as the user types in
 */
function validateFormFields(element) {
  $(element).valid();
  showSubmitBtnOnValidForm();
}

/**
 * This method enables the form 'submit' button if all required fields are filled-in,
 * and disables otherwise.
 */
function showSubmitBtnOnValidForm() {
  /*
   * valid if and only if:
   * 1. all required fields are filled in
   * 2. input values pass the validation requirements
   */
   const isValid = (isFormValid() && $('#elqForm').valid());
   $('.submit-btn').attr('disabled', !isValid);
}

/**
 * @return (boolean) : true if all required fields are filled in
 *                     false otherwise
 */
function isFormValid(validator) {
    const allBlankInputs = [...$('.form-input:pe-not-filled')];
    const visibleBlankInputs = allBlankInputs.filter((input) => {
      const isVisible = window.getComputedStyle(input, null)['visibility'];
      return isVisible === 'visible';
    });
    return !visibleBlankInputs.some(function(blankInput) {
      return blankInput.hasAttribute('required');
    });
}
